<?php

namespace App\Repository;

use App\Entity\ProductCategory;
use App\Entity\DataSearch;

use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;
use Doctrine\ORM\QueryBuilder;
use Doctrine\ORM\Query\ResultSetMapping;


class ProductCategoryRepository extends DataSearchRepository
{
    /**
     * Obtenir la liste des catégories de produits correspondant aux critères de recherche
     * 
     * @param   DataSearch  $search     Critères de sélection
     * 
     * @return  QueryBuilder
     */
    public function getProductCategoryQb(DataSearch $search=null){
        if(!$search){
            $search = new DataSearch();
        }

        $qb = $this->getEntityManager()->createQueryBuilder();

        $qb->select('category')
           ->from('App\Entity\ProductCategory', 'category');

        $this->addSearchCriteriaProductCategory($qb, $search);

        return $qb;
    }

    /**
     * Récupération de toutes les catégories de produits
     * 
     * @param   DataSearch      $search         Critères de sélection
     * 
     * @return array
     */
    public function getProductCategory(DataSearch $search=null) {

        $qb = $this->getProductCategoryQb($search);

        try {
            return $qb->getQuery()->execute();
        } catch (\Doctrine\ORM\NoResultException $e) {
            return null;
        }
    }

}
