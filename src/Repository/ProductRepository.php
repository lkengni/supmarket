<?php

namespace App\Repository;

use App\Entity\Product;
use App\Entity\DataSearch;

use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\QueryBuilder;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * @method Product|null find($id, $lockMode = null, $lockVersion = null)
 * @method Product|null findOneBy(array $criteria, array $orderBy = null)
 * @method Product[]    findAll()
 * @method Product[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ProductRepository extends DataSearchRepository
{
    /**
     * Obtenir la liste des catégories de produits correspondant aux critères de recherche
     * 
     * @param   DataSearch  $search     Critères de sélection
     * 
     * @return  QueryBuilder
     */
    public function getProductQb(DataSearch $search=null){
        if(!$search){
            $search = new DataSearch();
        }

        $qb = $this->getEntityManager()->createQueryBuilder();

        $qb->select('product, category')
           ->from('App\Entity\Product', 'product')
           ->leftJoin('product.category', 'category');

        $this->addSearchCriteriaProduct($qb, $search);
        $this->addSearchCriteriaProductCategory($qb, $search);

        return $qb;
    }

    /**
     * Ajouter les critères de recherche sur les produits dans la requête
     * 
     * @param QueryBuilder  $qb         Instance de QueryBuilder
     * @param DataSearch    $search     Critères de sélection 
     */
    public function addSearchCriteriaProduct(QueryBuilder $qb, DataSearch $search){
        if(!$this->aliasExists($qb, 'product')) {
            return;
        }

        // Identifiant
        $id = $search->get(DataSearch::PRODUCT_ID);
        if($id) {
            $qb->andWhere('product.id=:id')->setParameter('id', $id);
        }

        // Référence
        $ref = $search->get(DataSearch::PRODUCT_REF);
        if($ref) {
            $ref = '%' . $ref . '%';
            $qb->andWhere('product.reference LIKE :ref')->setParameter('ref', $ref);
        }

        // Nom
        $name = $search->get(DataSearch::PRODUCT_NAME);
        if($name) {
            $qb->andWhere('product.name=:name')->setParameter('name', $name);
        }
    }

    /**
     * Récupération de toutes les produits
     * 
     * @param   DataSearch      $search         Critères de sélection
     * 
     * @return array
     */
    public function getProduct(DataSearch $search=null) {

        $qb = $this->getProductQb($search);

        try {
            return $qb->getQuery()->execute();
        } catch (\Doctrine\ORM\NoResultException $e) {
            return null;
        }
    }
}
