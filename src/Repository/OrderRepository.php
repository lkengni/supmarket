<?php

namespace App\Repository;

use App\Entity\Order;
use App\Entity\Product;
use App\Entity\DataSearch;

use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\QueryBuilder;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * @method Order|null find($id, $lockMode = null, $lockVersion = null)
 * @method Order|null findOneBy(array $criteria, array $orderBy = null)
 * @method Order[]    findAll()
 * @method Order[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class OrderRepository extends DataSearchRepository
{
    /**
     * Obtenir la liste des catégories de produits correspondant aux critères de recherche
     * 
     * @param   DataSearch  $search     Critères de sélection
     * 
     * @return  QueryBuilder
     */
    public function getOrderQb(DataSearch $search=null){
        if(!$search){
            $search = new DataSearch();
        }

        $qb = $this->getEntityManager()->createQueryBuilder();

        $qb->select('orders')
           ->from('App\Entity\Order', 'orders');

        $this->addSearchCriteriaOrder($qb, $search);

        return $qb;
    }

    /**
     * Ajouter les critères de recherche sur les produits dans la requête
     * 
     * @param QueryBuilder  $qb         Instance de QueryBuilder
     * @param DataSearch    $search     Critères de sélection 
     */
    public function addSearchCriteriaOrder(QueryBuilder $qb, DataSearch $search){
        if(!$this->aliasExists($qb, 'orders')) {
            return;
        }

        // Référence
        $ref = $search->get(DataSearch::ORDER_REF);
        if($ref) {
            $ref = '%' . $ref . '%';
            $qb->andWhere('orders.reference LIKE :ref')->setParameter('ref', $ref);
        }
    }

    /**
     * Récupération de toutes les produits
     * 
     * @param   DataSearch      $search         Critères de sélection
     * 
     * @return array
     */
    public function getOrder(DataSearch $search=null) {

        $qb = $this->getOrderQb($search);

        try {
            return $qb->getQuery()->execute();
        } catch (\Doctrine\ORM\NoResultException $e) {
            return null;
        }
    }
}
