<?php

namespace App\Service;

use App\Entity\Product;
use App\Entity\Order;
use App\Entity\DataSearch;

use Doctrine\ORM\EntityManagerInterface;
use Psr\Log\LoggerInterface;

class OrderService 
{
    /**
     * Logger
     * 
     * @var Psr\Log\LoggerInterface; 
     */
    protected $logger;
    
    /**
     * Gestionnaire d'entité
     * 
     * @var EntityManager 
     */
    protected $em;
    
    /**
     * Constructeur
     * 
     * @param Logger                $logger
     * @param TranslatorInterface   $translator
     * @param EntityManager         $em
     */
    public function __construct(LoggerInterface $logger, EntityManagerInterface $em) {
        $this->logger       = $logger;
        $this->em           = $em;
    }

    /**
     * Récupérer une instance de recherche sur les commandes
     * 
     * @param   string          $ref        Filtre sur référence de la commande
     * 
     * @return  DataSearch          Instance avec les différents critères de recherche
     */
    public function getOrderSearch($ref=null) {
        $search = new DataSearch();
        
        $search->set(DataSearch::ORDER_REF, $ref);
        
        return $search;
    }

    /**
     * Récupérer les commandes correspondant aux critères de sélection
     * 
     * @param   DataSearch      $search     Critères de sélection
     * 
     * @return array    Tableau de commandes
     */
    public function getOrders(DataSearch $search=null) {
        return $this->em->getRepository('App:Order')->getOrder($search);
    }

}