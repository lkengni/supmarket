<?php

namespace App\Service;

use App\Entity\ProductCategory;
use App\Entity\DataSearch;

use Doctrine\ORM\EntityManagerInterface;
use Psr\Log\LoggerInterface;

class ProductCategoryService 
{
    /**
     * Logger
     * 
     * @var Psr\Log\LoggerInterface; 
     */
    protected $logger;
    
    /**
     * Gestionnaire d'entité
     * 
     * @var EntityManager 
     */
    protected $em;
    
    /**
     * Constructeur
     * 
     * @param Logger                $logger
     * @param TranslatorInterface   $translator
     * @param EntityManager         $em
     */
    public function __construct(LoggerInterface $logger, EntityManagerInterface $em) {
        $this->logger       = $logger;
        $this->em           = $em;
    }

    /**
     * Récupérer une instance de recherche sur les catégories de produits
     * 
     * @param integer   $id     Filtre sur identifiant de la catégorie
     * 
     * @return DataSearch       Instance avec les différents critères de recherche
     */
    public function getProductCategorySearch($id=null) {
        $search = new DataSearch();
        
        $search->set(DataSearch::PRODUCT_CATEGORY, $id);
        
        return $search;
    }

    /**
     * Récupérer les catégories de produits correspondant aux critères de sélection
     * 
     * @param   DataSearch      $search     Critères de sélection
     * 
     * @return array    Tableau de catégories
     */
    public function getProductCategories(DataSearch $search=null) {
        return $this->em->getRepository(ProductCategory::class)->getProductCategory($search);
    }


}