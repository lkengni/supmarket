<?php

namespace App\Service;

use App\Entity\Product;
use App\Entity\DataSearch;

use Doctrine\ORM\EntityManagerInterface;
use Psr\Log\LoggerInterface;

class ProductService 
{
    /**
     * Logger
     * 
     * @var Psr\Log\LoggerInterface; 
     */
    protected $logger;
    
    /**
     * Gestionnaire d'entité
     * 
     * @var EntityManager 
     */
    protected $em;
    
    /**
     * Constructeur
     * 
     * @param Logger                $logger
     * @param TranslatorInterface   $translator
     * @param EntityManager         $em
     */
    public function __construct(LoggerInterface $logger, EntityManagerInterface $em) {
        $this->logger       = $logger;
        $this->em           = $em;
    }

    /**
     * Récupérer une instance de recherche sur les catégories de produits
     * 
     * @param   integer         $id         Filtre sur identifiant de la catégorie
     * @param   string          $name       Filtre sur Nom du produit
     * @param   string          $ref        Filtre sur référence du produit
     * @param   ProductCategory $cat        Filtre sur catégorie de produit
     * 
     * @return  DataSearch          Instance avec les différents critères de recherche
     */
    public function getProductSearch($id=null, $name=null, $ref=null, $cat=null) {
        $search = new DataSearch();
        
        $search->set(DataSearch::PRODUCT_ID, $id);
        $search->set(DataSearch::PRODUCT_REF, $ref);
        $search->set(DataSearch::PRODUCT_NAME, $name);
        $search->set(DataSearch::PRODUCT_CATEGORY, $cat);
        
        return $search;
    }

    /**
     * Récupérer les produits correspondant aux critères de sélection
     * 
     * @param   DataSearch      $search     Critères de sélection
     * 
     * @return array    Tableau de produits
     */
    public function getProducts(DataSearch $search=null) {
        return $this->em->getRepository(Product::class)->getProduct($search);
    }

}