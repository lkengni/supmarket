<?php

namespace App\Controller;

use App\Entity\Product;
use App\Entity\DataSearch;
use App\Form\ProductType;

use Symfony\Component\HttpFoundation\Request;

class ProductController extends MasterController
{
    /**
     * Affichage de la page recensant les produits
     * 
     * @param   Request     $request    Requête HTTP
     * 
     * @return  Response
     */
    public function index(Request $request){
        $this->denyAccessUnlessGranted(array('ROLE_VENDEUR'));

        // Récupérer tous les produits
        $search     = $this->getProductService()->getProductSearch();
        // On instancie le formulaire de recherche
        $form       = $this->getSearchForm($request, $search);

        $products   = $this->getProductService()->getProducts($search);

        $parameters = array(
            'form'      => $form->createView(),
            'products'  =>  $products
        );

        return $this->render('product/index.html.twig', $parameters);
    }

    /**
     * Ajout / Édition d'une produit
     * 
     * @param   Request     $request    Requête HTTP
     * @param   int         $id         Identifiant
     * 
     * @return  Response
     */
    public function edit(Request $request, int $id=null){
        $this->denyAccessUnlessGranted(array('ROLE_VENDEUR'));

        if($id) {
            $mode       = 'update';

            $search     = $this->getProductService()->getProductSearch($id);
            $product    = $this->getProductService()->getProducts($search) ? $this->getProductService()->getProducts($search)[0] : new Product();
        }
        else {
            $mode       = 'new';

            $product    = new Product();
        }

        $form       = $this->createForm(ProductType::class, $product);

        $form->handleRequest($request);

        if($form->isSubmitted() && $form->isValid()) {
            $this->saveProduct($product, $mode);

            return $this->redirectFormToRoute('app_product_edit', array('id' => $product->getId()));
        }

        $parameters = array(
            'form'      => $form->createView(),
            'product'   => $product,
            'mode'      => $mode
        );

        return $this->render('product/edit.html.twig', $parameters);
    }

    /**
     * Compléter le produit avec des informations avant enregistrement
     * 
     * @param   Product     $product
     * @param   string      $mode 
     * 
     * @return Product
     */
    private function completeProductBeforeSave(Product $product, string $mode) {
        if($mode == 'new'){
            $product->setCreatedAt(new \DateTime());
            $product->setCreatedBy($this->getUser());
        }
        $product->setUpdatedAt(new \DateTime());
        $product->setUpdatedBy($this->getUser());

        return $product;
    }

    /**
     * Enregistrer un produit en base de données
     * 
     * @param   Product     $product
     * @param   string      $mode 
     */
    private function saveProduct(Product $product, string $mode){
        $product = $this->completeProductBeforeSave($product, $mode);
        
        $em = $this->getDoctrine()->getManager();
        $em->persist($product);
        $em->flush();
        $this->addFlash('success', 'Enregistré avec succès');
    }
}