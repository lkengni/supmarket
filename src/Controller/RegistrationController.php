<?php

namespace App\Controller;

use App\Entity\User;
use App\Form\RegistrationFormType;
use App\Security\UserAuthenticator;
use Octopush\Api\Client;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Symfony\Component\Security\Guard\GuardAuthenticatorHandler;

class RegistrationController extends AbstractController
{
    /**
     * Mailer
     */
    private $mailer;

    /**
     * Encodeur de mot de passe
     */
    private $passwordEncoder;

    /**
     * GuardHandler
     */
    private $guardHandler;

    /**
     * Authentificateur
     */
    private $authenticator;


    /**
     * Constructeur
     */
    public function __construct(
                                UserPasswordEncoderInterface $passwordEncoder, 
                                GuardAuthenticatorHandler $guardHandler, 
                                UserAuthenticator $authenticator, 
                                \Swift_Mailer $mailer
                                )
    {
        $this->mailer                   = $mailer;
        $this->passwordEncoder          = $passwordEncoder;
        $this->guardHandler             = $guardHandler;
        $this->authenticator            = $authenticator;
    }

    /**
     * Inscription d'un utilisateur
     */
    public function register(Request $request): Response
    {
        if ($this->getUser()) {
            return $this->redirectToRoute('app_index');
        }
        
        $user = new User();
        $form = $this->createForm(RegistrationFormType::class, $user);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            // Encodage du mot de passe
            /*$user->setPassword(
                $passwordEncoder->encodePassword(
                    $user,
                    $form->get('plainPassword')->getData()
                )
            ); */
            $this->processChangePassword($user, $form->get('plainPassword')->getData(), $this->passwordEncoder);

            $user->setRoles(array('ROLE_VENDEUR'));
            $user->getAccount() ?? $user->setAccount(0);

            try{
                $this->saveUser($user);

                // Envoi de mail
                $title      = "Confirmation d'inscription";
                $template   = 'emails/registration.html.twig';
                $params     = ['user' => $user];
                $this->sendMail($user, $title, $template, $params, $this->mailer);
            }
            catch(\Exception $e){
                throw new \Exception("Echec création utilisateur ! : " . $e->getMessage());
            }

            return $this->guardHandler->authenticateUserAndHandleSuccess(
                $user,
                $request,
                $this->authenticator,
                'main' // firewall name in security.yaml
            );
        }

        // Paramètres éventuels à passer à la vue
        $parameters = array(
            'form' => $form->createView(),
        );

        return $this->render('registration/register.html.twig', $parameters);
    }

    /**
     * Demander la réinitialisation d'un mot de passe
     * 
     * @param   Request         $request    Requête HTTP
     * @param   \Swift_Mailer   $mailer     Mailer
     * 
     * @return  Response
     */
    public function requestPassword(Request $request, \Swift_Mailer $mailer)
    {
        // Paramètres éventuels à passer à la vue
        $parameters = array(
            'success'   => null,
            'error'     => null
        );

        if($request){
            $repository = $this->getDoctrine()->getRepository(User::class);
            $user       = $repository->findOneby(['username' => $request->request->get('username')]);

            if($user){
                // On génère un token et on l'affecte à l'utilisateur
                $token = hash('sha256', random_bytes(32));
                $user->setPasswordToken($token);
                $this->saveUser($user);

                // Ensuite il reçoit par mail un lien pour réinitialiser le mot de passe
                $title      = "Réinitialisation du mot de passe";
                $template   = 'emails/request_password.html.twig';
                $params     = ['user' => $user];

                try{
                    $this->sendMail($user, $title, $template, $params, $this->mailer);

                    $parameters['success'] = 'Un email vous a été envoyé. Veuillez consulter votre boite email.';
                } catch(Exception $e){
                    $parameters['error'] = 'Un problème est survenu. L\'email n\'a pas pu être envoyé';
                }
            }
        }

        return $this->render('resetting/request_password.html.twig', $parameters);
    }

    /**
     * Réinitialisation du mot de passe d'un utilisateur
     * 
     * @param   Request                         $request            Requête HTTP
     * @param   UserPasswordEncoderInterface    $passwordEncoder    Encodeur
     * @param   string                          $token              Token unique pour réinitialisation
     * @param   \Swift_Mailer                   $mailer             Mailer
     * 
     * @return  Response
     */
    public function resetPassword(Request $request, UserPasswordEncoderInterface $passwordEncoder, string $token, \Swift_Mailer $mailer)
    {
        if(!$token){
            // Si aucun token n'est spécifié, il ne peut pas accéder à la page
            return $this->redirectToRoute('request_password');
        }

        $repository = $this->getDoctrine()->getRepository(User::class);
        $user       = $repository->findOneby(['passwordToken' => $token]);
        $error      = null;

        if(!$user){
            return $this->redirectToRoute('request_password');
        }
        
        if($_POST){
            $password   = $request->request->get('password');
            $cfPassword = $request->request->get('confirm-password');

            if($password == $cfPassword){
                $this->processChangePassword($user, $password, $passwordEncoder);
                $user->setPasswordToken(null);
                $this->saveUser($user);

                // Un mail est envoyé pour confirmer que le mot de passe est modifié
                $title      = "Mot de passe réinitialisé avec succès";
                $template   = 'emails/reset_password_success.html.twig';
                $params     = ['user' => $user];
                $this->sendMail($user, $title, $template, $params, $mailer);

                return $this->render('resetting/reset_complete.html.twig');
            }
            else{
                $error = 'Les mots de passe ne sont pas identiques.';
            }
        }

        // Paramètres éventuels à passer à la vue
        $parameters = array(
            'error'     => $error
        );

        return $this->render('resetting/reset_password.html.twig', $parameters);
    }

    /**
     * Mise à jour d'un mot de passe utilisateur
     * 
     * @param   User                            $user               Utilisateur
     * @param   string                          $password           Nouveau mot de passe
     * @param   UserPasswordEncoderInterface    $passwordEncoder    Encodeur
     * 
     * @return  User
     */
    public function processChangePassword(User $user, string $password, UserPasswordEncoderInterface $passwordEncoder){
        $user->setPassword(
            $passwordEncoder->encodePassword(
                $user,
                $password
            )
        );

        return $user;
    }

    /**
     * Enregistrer un utilisateur en base de données
     * 
     * @param   User    $user
     */
    private function saveUser(User $user){
        $entityManager = $this->getDoctrine()->getManager();
        $entityManager->persist($user);
        $entityManager->flush();
    }

    /**
     * Envoi d'email
     * 
     * @param   User            $user       Utilisateur
     * @param   string          $title      Titre du mail
     * @param   string          $template   Template à utiliser pour le corps du mail
     * @param   array           $params     Paramètres à passer à la vue
     * @param   \Swift_Mailer   $mailer     Mailer
     */
    public function sendMail(User $user, string $title, string $template, array $params, \Swift_Mailer $mailer) {
        $message = (new \Swift_Message('SupMarket - ' . $title))
            ->setFrom('no-reply@supmarket.fr')
            ->setTo($user->getEmail())
            ->setBody(
                $this->renderView(
                    $template,
                    $params
                ),
                'text/html'
            )
        ;

        $mailer->send($message);
    }


}
