<?php

namespace App\Controller\WebService;

use App\Entity\User;
use Symfony\Component\Security\Core\Authorization\AuthorizationCheckerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class RegistrationController extends MasterController
{
    /**
     * Mailer
     */
    private $mailer;

    /**
     * Encodeur de mot de passe
     */
    private $passwordEncoder;

    /**
     * Vérificateur d'autorisation
     */
    private $authChecker;


    /**
     * Constructeur
     */
    public function __construct(UserPasswordEncoderInterface $passwordEncoder, AuthorizationCheckerInterface $authChecker, \Swift_Mailer $mailer)
    {
        $this->mailer           = $mailer;
        $this->passwordEncoder  = $passwordEncoder;
        $this->authChecker      = $authChecker;
    }

    /**
     * Inscription d'un utilisateur
     * 
     * @param   Request     $request    Requête HTTP
     * 
     * @return  Response
     */
    public function register(Request $request)
    {
        // Array contenant toute les données reçues via la requête HTTP
        $data = json_decode($request->getContent(), true);
        $user = new User();

        if ($data && count($data) >= 5 && $data['username']) {
            $user1 = $this->getRepository('App:User')->findby(['username' => $data['username']]);

            if($user1){
                return new JsonResponse([
                    'code'          => 401,
                    'message'       => 'This user already exists'
                ]);
            }

            $user->setName($data['name']);
            $user->setFirstName($data['first_name']);
            $user->setEmail($data['email']);
            $user->setUsername($data['username']);
            $user->setCardNumber($data['card_number']);
            $d = new \DateTime($data['card_expiration']);
            $user->setCardExpirationDate($d);
            $user->setCardCvv($data['card_cvv']);
            $user->setAdresse($data['adresse']);
            $d = new \DateTime($data['birth_date']);
            $user->setBirthDate($d);
            $user->setRoles(array('ROLE_CLIENT'));

            $this->processChangePassword($user, $data['password'], $this->passwordEncoder);

            try{
                $this->saveUser($user);

                // Envoi de mail
                $title      = "Confirmation d'inscription"; 
                $template   = 'emails/registration.html.twig';
                $params     = ['user' => $user];
                $this->sendMail($user, $title, $template, $params, $this->mailer);

                return new JsonResponse([
                    'code'          => 200,
                    'message'       => 'User created succesfully !'
                ]);
            }
            catch(\Exception $e){
                return new JsonResponse([
                    'code'          => 500,
                    'message'       => 'Internal error !'
                ]);
            }
        }
        else {
            return new JsonResponse([
                'code'          => 404,
                'message'       => 'Data missing !'
            ]);
        }
        
    }

    /**
     * Mise à jour d'un mot de passe utilisateur
     * 
     * @param   User                            $user               Utilisateur
     * @param   string                          $password           Nouveau mot de passe
     * @param   UserPasswordEncoderInterface    $passwordEncoder    Encodeur
     * 
     * @return  User
     */
    public function processChangePassword(User $user, string $password, UserPasswordEncoderInterface $passwordEncoder){
        $user->setPassword(
            $passwordEncoder->encodePassword(
                $user,
                $password
            )
        );

        return $user;
    }

    /**
     * Envoi d'email
     * 
     * @param   User            $user       Utilisateur
     * @param   string          $title      Titre du mail
     * @param   string          $template   Template à utiliser pour le corps du mail
     * @param   array           $params     Paramètres à passer à la vue
     * @param   \Swift_Mailer   $mailer     Mailer
     */
    /* public function sendMail(User $user, string $title, string $template, array $params, \Swift_Mailer $mailer) {
        $message = (new \Swift_Message('SupMarket - ' . $title))
            ->setFrom('no-reply@supmarket.fr')
            ->setTo($user->getEmail())
            ->setBody(
                $this->renderView(
                    $template,
                    $params
                ),
                'text/html'
            )
        ;

        $mailer->send($message);
    } */


}
