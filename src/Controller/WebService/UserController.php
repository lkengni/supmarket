<?php

namespace App\Controller\WebService;

use App\Entity\User;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

class UserController extends MasterController 
{
    /**
     * Mailer
     */
    protected $mailer;

    /**
     * Constructeur
     */
    public function __construct(\Swift_Mailer $mailer)
    {
        $this->mailer = $mailer;
    }


    /**
     * Récupérer les informations de l'utilisateur authentifié
     * 
     * @param   Request     $request    Requête HTTP
     * 
     * @return  Response
     */
    public function getUserInfo(Request $request){
        $user = $this->getUser();

        return new JsonResponse([
            'code'              => 200,
            'username'          => $user->getUsername(),
            'name'              => $user->getName(),
            'first_name'        => $user->getFirstName(),
            'email'             => $user->getEmail(),
            'birth_date'        => $user->getBirthDate()->format('d/m/Y'),
            'adresse'           => $user->getAdresse(),
            'card_number'       => $user->getCardNumber(),
            'card_expiration'   => $user->getCardExpirationDate()->format('d/m/Y'),
            'card_cvv'          => $user->getCardCvv(),
            'solde'             => $user->getAccount()
        ]);
    }

    /**
     * Mettre à jour les informations bancaires de l'utilisateur
     * 
     * @param   Request     $request    Requête HTTP
     * 
     * @return  Response
     */
    public function updateCardInfo(Request $request){
        $user = $this->getUser();
        // Array contenant toute les données reçues via la requête HTTP
        $data = json_decode($request->getContent(), true);

        try {
            $user->setCardNumber($data['card_number']);
            $d = new \DateTime($data['card_expiration']);
            $user->setCardExpirationDate($d);
            $user->setCardCvv($data['card_cvv']);

            $this->saveUser($user);

            // Email de confirmation
            $this->sendMail($user, 'Paiement', 'emails/card_update.html.twig', ['user' => $user], $this->mailer);

            return new JsonResponse([
                'code'      => 200,
                'message'   => 'Informations bancaires mises à jour'
            ]);
        } catch(\Exception $e){
            return new JsonResponse([
                'code'      => 500,
                'message'   => 'Erreur interne'
            ]);
        }
    }

    /**
     * Mettre à jour le solde de l'utilisateur
     * 
     * @param   Request         $request    Requête HTTP
     * @param   \Swift_Mailer   $mailer
     * 
     * @return  Response
     */
    public function updateAccount(Request $request, \Swift_Mailer $mailer){
        $user = $this->getUser();
        // Array contenant toutes les données reçues via la requête HTTP
        $data = json_decode($request->getContent(), true);

        try {
            if($user->getCardNumber() && $user->getCardExpirationDate() && $user->getCardCvv()){
                $newAccount = intval($user->getAccount()) + intval($data['amount']);
                $user->setAccount($newAccount);

                $this->saveUser($user);

                // Email de confirmation
                $this->sendMail($user, 'Mise à jour solde', 'emails/account_update.html.twig', ['user' => $user], $mailer);

                return new JsonResponse([
                    'code'      => 200,
                    'message'   => 'Solde mis à jour'
                ]);
            }
            else {
                return new JsonResponse([
                    'code'      => 500,
                    'message'   => 'Moyen de paiement invalide'
                ]);
            }
            
        } catch(\Exception $e){
            return new JsonResponse([
                'code'      => 500,
                'message'   => 'Erreur interne'
            ]);
        }
    }

}