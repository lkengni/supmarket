<?php

namespace App\Controller\WebService;

use App\Entity\Order;
use App\Entity\User;
use App\Entity\Product;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

class OrderController extends MasterController 
{
    /**
     * Mailer
     */
    protected $mailer;

    /**
     * Constructeur
     */
    public function __construct(\Swift_Mailer $mailer)
    {
        $this->mailer = $mailer;
    }
    

    /**
     * Récupérer les informations de l'utilisateur authentifié
     * 
     * @param   Request     $request    Requête HTTP
     * 
     * @return  Response
     */
    public function buy(Request $request){
        $user = $this->getUser();
        // Array contenant toutes les données reçues via la requête HTTP
        $data = json_decode($request->getContent(), true);

        try {
            
            $products   = $this->getRepository('App:Product')->findBy(['reference' => $data['references']]);
            $arrFinal   = array();
            $total_ttc  = 0;
            $i          = 0;

            // Mise à jour des quantités de chaque produit
            foreach($products as $product){
                $arr    = array();
                $qte    = $product->getQuantity() - intval($data['quantities'][$i]);
                $product->setQuantity($qte);

                // On enregistre le produit en BDD
                $this->saveProduct($product);

                $arr['product']     = $product;
                $arr['quantity']    = $data['quantities'][$i];
                array_push($arrFinal, $arr);

                $total_ttc += $product->getPrice() * $data['quantities'][$i];
                $i += 1;
            }

            // On vérifie le solde de l'utilisateur 
            if($user->getAccount() < $total_ttc){
                return new JsonResponse([
                    'code'          => 401,
                    'message'       => 'Solde insuffisant'
                ]);
            }
            else {
                $newAccount = intval($user->getAccount()) - $total_ttc;
                $user->setAccount($newAccount);
                
                $order = new Order();
                $order->setProducts($arrFinal);
                $order->setReference(random_int(10000000, 19999999));
                $order->setTotalTtc($total_ttc);
                $order->setTotalHt($total_ttc / 1.2);
                $order->setTva(0.2);

                // Enregistrement de la commande
                $this->saveOrder($order, 'new');
                // Mise à jour de l'utilisateur
                $this->saveUser($user);

                // Email de confirmation
                $params = [
                    'user'      => $user,
                    'products'  => $arrFinal,
                    'quantity'  => $data['quantities'],
                    'order'     => $order
                ];
                $this->sendMail($user, 'Confirmation d\'achat', 'emails/order_confirmation.html.twig', $params, $this->mailer);

                return new JsonResponse([
                    'code'          => 200,
                    'references'    => sizeof($arrFinal),
                    'message'       => 'OK ! Achat effectué'
                ]);
            }
            
        } catch(\Exception $e){
            return new JsonResponse([
                'code'          => 500,
                'message'       => 'Une erreur interne est survenue',
                'erreur'        => $e->getMessage()
            ]);
        }
    }

    /**
     * Lister les commandes utilisateur
     * 
     * @param   Request     $request    Requête HTTP
     * 
     * @return  Response
     */
    public function listOrders(Request $request){
        $user   = $this->getUser();

        try{
            $orders = $user->getOrders();
            $refs   = array();

            foreach($orders as $order){
                array_push($refs, $order->getReference());
            }

            return new JsonResponse([
                'code'          => 200,
                'references'    => $refs,
            ]);
        } catch(\Exception $e){
            return new JsonResponse([
                'code'          => 500,
                'message'       => 'Une erreur interne est survenue',
                'exception'     => $e->getMessage()
            ]);
        }
    }

    /**
     * Récupérer une commande
     * 
     * @param   Request     $request    Requête HTTP
     * @param   int         $ref        Référence
     * 
     * @return  Response
     */
    public function getOrder(Request $request, int $ref){
        try{
            $order  = $this->getRepository('App:Order')->findOneBy(['reference' => $ref]);
            $arr    = array();
            $i      = 0;

            $arr                        = array();
            $arr['reference']           = $ref;
            $arr['total']               = $order->getTotalTtc();
            $arr['product_names']       = array();
            $arr['product_quantities']  = array();
            $arr['product_prices']      = array();
            foreach($order->getProducts() as $pr){
                array_push($arr['product_names'], $pr['product']->getName());
                array_push($arr['product_prices'], $pr['product']->getPrice());
                array_push($arr['product_quantities'], $pr['quantity']);
                
                $i += 1;
            }

            return new JsonResponse([
                'code'  => 200,
                'order' => $arr,
            ]);
        } catch(\Exception $e){
            return new JsonResponse([
                'code'          => 500,
                'message'       => 'Une erreur interne est survenue',
                'exception'     => $e->getMessage()
            ]);
        }
    }


    /**
     * Compléter le produit avec des informations avant enregistrement
     * 
     * @param   Order       $order
     * @param   string      $mode 
     * 
     * @return Order
     */
    private function completeOrderBeforeSave(Order $order, string $mode) {
        if($mode == 'new'){
            $order->setCreatedAt(new \DateTime());
            $order->setCreatedBy($this->getUser());
        }
        $order->setUpdatedAt(new \DateTime());
        $order->setUpdatedBy($this->getUser());

        return $order;
    }

    /**
     * Enregistrer un produit en base de données
     * 
     * @param   Order       $order
     * @param   string      $mode 
     */
    private function saveOrder(Order $order, string $mode){
        $order = $this->completeOrderBeforeSave($order, $mode);
        
        $em = $this->getDoctrine()->getManager();
        $em->persist($order);
        $em->flush();
    }

}