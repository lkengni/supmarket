<?php

namespace App\Controller\WebService;

use App\Entity\User;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

class IndexController extends AbstractController 
{
    /**
     * Afficher la documentation de l'API
     */
    public function index(Request $request){
        $this->denyAccessUnlessGranted(array('ROLE_ADMIN'));

        return $this->render('api/index.html.twig');
    }

}