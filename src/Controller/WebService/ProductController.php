<?php

namespace App\Controller\WebService;

use App\Entity\Product;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

class ProductController extends MasterController 
{
    /**
     * Récupérer les informations de l'utilisateur authentifié
     * 
     * @param   Request     $request    Requête HTTP
     * 
     * @return  Response
     */
    public function getProducts(Request $request){
        // Récupérer tous les produits
        $search     = $this->getProductService()->getProductSearch();
        // On instancie le formulaire de recherche
        $form       = $this->getSearchForm($request, $search);

        try {
            $products   = $this->getProductService()->getProducts($search);
            $arr        = array();
            $i          = 1;

            foreach($products as $product){
                $arr1['reference']      = $product->getReference();
                $arr1['name']           = $product->getName();
                $arr1['description']    = $product->getDescription();
                $arr1['quantity']       = $product->getQuantity();
                $arr1['price']          = $product->getPrice();
                $arr1['category_id']    = $product->getCategory()->getId();
                $arr1['category_name']  = $product->getCategory()->getName();

                $arr[$i] = $arr1;
                $i++;
            }

            return new JsonResponse([
                'code'          => 200,
                'message'       => 'OK',
                'products'      => $arr,
            ]);
        } catch(\Exception $e){
            return new JsonResponse([
                'code'          => 500,
                'message'       => 'Une erreur interne est survenue',
            ]);
        }
    }

}