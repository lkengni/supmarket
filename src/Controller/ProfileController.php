<?php

namespace App\Controller;

use App\Entity\User;

class ProfileController extends MasterController
{
    /**
     * Affichage de la page recensant les informations utilisateur
     * 
     * @return  Response
     */
    public function index(){
        $this->denyAccessUnlessGranted(array('ROLE_VENDEUR'));

        $parameters = array(
            'user'  =>  $this->getUser()
        );

        return $this->render('profile/index.html.twig', $parameters);
    }
}