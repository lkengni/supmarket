<?php

namespace App\Controller;

use App\Entity\Product;
use App\Entity\Order;
use App\Entity\DataSearch;
use App\Form\ProductType;

use Symfony\Component\HttpFoundation\Request;

class OrderController extends MasterController
{
    /**
     * Affichage de la page recensant les produits
     * 
     * @param   Request     $request    Requête HTTP
     * 
     * @return  Response
     */
    public function index(Request $request){
        $this->denyAccessUnlessGranted(array('ROLE_VENDEUR'));

        // Récupérer tous les produits
        $search     = $this->getOrderService()->getOrderSearch();
        // On instancie le formulaire de recherche
        $form       = $this->getSearchForm($request, $search);

        $orders     = $this->getOrderService()->getOrders($search);

        $parameters = array(
            'form'      => $form->createView(),
            'orders'    => $orders
        );

        return $this->render('order/index.html.twig', $parameters);
    }

    /**
     * Ajout / Édition d'une produit
     * 
     * @param   Request     $request    Requête HTTP
     * @param   int         $ref        Référence
     * 
     * @return  Response
     */
    public function listDetails(Request $request, int $ref=null){
        $this->denyAccessUnlessGranted(array('ROLE_VENDEUR'));

        $search = $this->getOrderService()->getOrderSearch($ref);
        $order  = $this->getOrderService()->getOrders($search);

        $parameters = array(
            'order'     => $order[0],
            'products'  => $order[0]->getProducts()
        );

        return $this->render('order/liste.html.twig', $parameters);
    }
}