<?php

namespace App\Controller;

use App\Entity\ProductCategory;
use App\Entity\DataSearch;
use App\Form\ProductCategoryType;

use Symfony\Component\HttpFoundation\Request;

class ProductCategoryController extends MasterController
{
    /**
     * Affichage de la page recensant les catégories de produits
     * 
     * @param   Request     $request    Requête HTTP
     * 
     * @return  Response
     */
    public function index(Request $request){
        $this->denyAccessUnlessGranted(array('ROLE_VENDEUR'));

        // Récupérer toutes les catégories de produits
        $search     = $this->getProductCategoryService()->getProductCategorySearch();
        // On instancie le formulaire de recherche
        $form       = $this->getSearchForm($request, $search);

        $categories = $this->getProductCategoryService()->getProductCategories($search);

        $parameters = array(
            'form'          => $form->createView(),
            'categories'    => $categories
        );

        return $this->render('productCategory/index.html.twig', $parameters);
    }

    /**
     * Ajout / Édition d'une catégorie
     * 
     * @param   Request     $request    Requête HTTP
     * @param   int         $id         Identifiant
     * 
     * @return  Response
     */
    public function edit(Request $request, int $id=null){
        $this->denyAccessUnlessGranted(array('ROLE_VENDEUR'));

        if($id) {
            $mode       = 'update';

            $search     = $this->getProductCategoryService()->getProductCategorySearch($id);
            $category   = $this->getProductCategoryService()->getProductCategories($search)[0];
        }
        else {
            $mode       = 'new';

            $category   = new ProductCategory();
        }

        $form       = $this->createForm(ProductCategoryType::class, $category);

        $form->handleRequest($request);

        if($form->isSubmitted() && $form->isValid()) {
            $this->saveProductCategory($category, $mode);

            return $this->redirectFormToRoute('app_product_category_edit', array('id' => $category->getId()));
        }

        $parameters = array(
            'form'  => $form->createView(),
            'cat'   => $category,
            'mode'  => $mode
        );

        return $this->render('productCategory/edit.html.twig', $parameters);
    }

    /**
     * Compléter la catégorie avec des informations avant enregistrement
     * 
     * @param   ProductCategory     $category
     * @param   string              $mode 
     * 
     * @return ProductCategory
     */
    private function completeProductCategoryBeforeSave(ProductCategory $category, string $mode) {
        if($mode == 'new'){
            $category->setCreatedAt(new \DateTime());
            $category->setCreatedBy($this->getUser());
        }
        $category->setUpdatedAt(new \DateTime());
        $category->setUpdatedBy($this->getUser());

        return $category;
    }

    /**
     * Enregistrer une catégorie en base de données
     * 
     * @param   ProductCategory     $category
     * @param   string              $mode 
     */
    private function saveProductCategory(ProductCategory $category, string $mode){
        $category = $this->completeProductCategoryBeforeSave($category, $mode);
        
        $em = $this->getDoctrine()->getManager();
        $em->persist($category);
        $em->flush();
        $this->addFlash('success', 'Enregistré avec succès');
    }
}