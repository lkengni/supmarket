<?php

namespace App\Controller;

use App\Entity\Order;
use App\Entity\User;
use App\Entity\Product;

class HomeController extends MasterController
{
    /**
     * Afficher la page d'accueil
     * 
     * @return  Response
     */
    public function index() {
        $this->denyAccessUnlessGranted(array('ROLE_VENDEUR'));

        $search = $this->getOrderService()->getOrderSearch();
        $orders = $this->getOrderService()->getOrders($search);
        $searchCat = $this->getProductCategoryService()->getProductCategorySearch();
        $categories = $this->getProductCategoryService()->getProductCategories($searchCat);
        $users = $this->getRepository('App:User')->findAll();

        $arrCat = array();
        $arr = array();
        $nbrs = array();
        $user_list = array();
        $user_nb   = array();

        foreach($orders as $order){
            $product = $order->getProducts()[0];
            //foreach($order->getProducts() as $product){
            $orderCatId = $product['product']->getCategory()->getId();
            $index = '';
            //array_push($arr, $product['product']->getCategory());

            foreach($categories as $cat){
                if($cat->getId() == $orderCatId){
                    $index = $cat->getName();
                    break;
                }
            }
            $arr[$index] = $arr[$index] ?? array();

            array_push($arrCat, $index);
            
            $arrCat = array_unique($arrCat);
            array_push($arr[$index], $product['product']->getName());
            //}
        }

        foreach($arr as $nb){
            array_push($nbrs, count($nb));
        }

        foreach($users as $user){
            if(count($user->getOrders()) > 0){
                array_push($user_list, $user->getUsername());
                array_push($user_nb, count($this->getUser()->getOrders()));
            }
        }

        $parameters = array(
            'cats' => $arrCat,
            'nbrs' => $nbrs,
            'nb_order' => $user_nb,
            'user_list' => $user_list
        );

        return $this->render('master/index.html.twig', $parameters);
    }
    
}
