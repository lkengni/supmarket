<?php

namespace App\Controller;

use App\Entity\User;
use App\Entity\DataSearch;
use App\Form\DataSearchType;
use App\Service\ProductCategoryService;
use App\Service\ProductService;
use App\Service\OrderService;
use Octopush\Api\Client;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

class MasterController extends AbstractController
{
    /**
     * ProductCategoryService
     */
    private $productCategoryService;

    /**
     * ProductService
     */
    private $productService;

    /**
     * OrderService
     */
    private $orderService;


    /**
     * Constructeur
     */
    public function __construct(
                                ProductCategoryService $productCategoryService, 
                                ProductService $productService,
                                OrderService $orderService)
    {
        $this->productCategoryService   = $productCategoryService;
        $this->productService           = $productService;
        $this->orderService             = $orderService;
    }

    /**
     * Récupérer un repository
     * 
     * @param string    $name      Nom du repository
     * 
     * @return EntityRepository
     */
    public function getRepository($name) {
        return $this->getDoctrine()->getManager()->getRepository($name);
    }


    /**
     * Envoi d'email
     * 
     * @param   User            $user       Utilisateur
     * @param   string          $title      Titre du mail
     * @param   string          $template   Template à utiliser pour le corps du mail
     * @param   array           $params     Paramètres à passer à la vue
     * @param   \Swift_Mailer   $mailer     Mailer
     */
    public function sendMail(User $user, string $title, string $template, array $params, \Swift_Mailer $mailer) {
        $message = (new \Swift_Message('SupMarket - ' . $title))
            ->setFrom('no-reply@supmarket.fr')
            ->setTo($user->getEmail())
            ->setBody(
                $this->renderView(
                    $template,
                    $params
                ),
                'text/html'
            )
        ;

        $this->mailer->send($message);
    }

    /**
     * Récupérer une instance du service ProductCategoryService
     * 
     * @return  ProductCategoryService
     */
    public function getProductCategoryService(){
        return $this->productCategoryService;
    }

    /**
     * Récupérer une instance du service ProductService
     * 
     * @return  ProductService
     */
    public function getProductService(){
        return $this->productService;
    }

    /**
     * Récupérer une instance du service OrderService
     * 
     * @return  OrderService
     */
    public function getOrderService(){
        return $this->orderService;
    }

    /**
     * Instancier le formulaire de recherche à partir des paramètres présents dans la requête HTTP
     * Et modifier en conséquence la recherche
     * 
     * @param Request       $request    Instance de la requête HTTP
     * @param DataSearch    $search     Critères de recherche initiaux
     * 
     * @return Form
     */
    public function getSearchForm(Request $request, DataSearch $search) {
        // Il est possible que le formulaire de recherche n'est pas été traité dans le cas d'un POST d'un autre formulaire
        // Car le formulaire de recherche intercepte la méthode GET
        // Néanmoins si les critères de recherche sont dans l'URL, il faut les intercepter
        if($request->isMethod('POST')) {
            $arr = $request->get('ds');
            foreach ($search->keys() as $key) {
                if(array_key_exists($key, $arr)) {
                    $v = DataSearch::cast($key, $arr[$key]);
                    $search->set($key, $v);
                }
            }
        }
        
        $form = $this->createForm(DataSearchType::class, $search->all());
        $form->handleRequest($request);
        
        $search->add($form->getData());
        
        return $form;
    }

    /**
     * Returns a RedirectResponse to the given route with the given parameters.
     *
     * @param string $route      The name of the route
     * @param array  $parameters An array of parameters
     * @param int    $status     The status code to use for the Response
     *
     * @return RedirectResponse
     */
    protected function redirectFormToRoute($route, array $parameters = array(), $status = 302) {
        return $this->redirectToRoute($route, $parameters, $status);
    }

    /**
     * Enregistrer un utilisateur en base de données
     * 
     * @param   User        $user
     */
    private function saveUser(User $user){
        $em = $this->getDoctrine()->getManager();
        $em->persist($user);
        $em->flush();
    }
    
}
