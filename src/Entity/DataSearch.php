<?php

namespace App\Entity;

use Symfony\Component\HttpFoundation\ParameterBag;

class DataSearch extends ParameterBag
{
    /**
     * Clés de recherche
     * Et valeurs prédéfinies
     */
    const WHATEVER          = 'whatever';

    // Clés sur l'entité Product
    const PRODUCT_ID        = 'product_id';
    const PRODUCT_REF       = 'product_ref';
    const PRODUCT_NAME      = 'product_name';

    // Clés sur l'entité ProductCategory
    const PRODUCT_CATEGORY  = 'product_category';

    // Clés sur l'entité Commande
    const ORDER_REF         = 'order_ref';
    
    /**
     * Pour récupérer les valeurs dans un formulaire
     * 
     * @param string $key
     * 
     * @return mixed
     */
    public function __get($key) {
        return $this->get($key);
    }

    /**
     * Vérifie qu'au moins une clé a été définie
     * 
     * @return boolean
     */
    public function isNotEmpty(){
        for($i = 0; $i < count($this->keys()); $i++){
            if($this->get($this->keys()[$i])){
                return true;
            }
        }

        return false;
    }
}
