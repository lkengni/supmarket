<?php

namespace App\Form;

use App\Entity\DataSearch;
use App\Service\ProductCategoryService;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\Length;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;


class DataSearchType extends AbstractType
{
    /**
     * Instance du service en charge des catégories de produits
     * 
     * @var ProductCategoryService 
     */
    private $productCategoryService;
    
    /**
     * Constructeur
     */
    public function __construct(ProductCategoryService $productCategoryService) {
        $this->productCategoryService = $productCategoryService;
    }

    public function buildForm(FormBuilderInterface $builder, array $options) {
       
        // On spécifie la méthode GET pour que la recherche et le tri puissent fonctionner ensemble
        $builder->setMethod('GET');
        
        // Les champs sont affichés en fonction des critères de recherche paramétrés dans l'objet DataSearch
        // Pour éviter des requêtes SQL inutiles quand le champ de recherche n'est pas affiché dans le formulaire
        // Pour éviter l'envoi de la donnée en GET quand le champ de recherche n'est pas affiché dans le formulaire
        $builder->addEventListener(FormEvents::PRE_SET_DATA, function (FormEvent $event) {
            $search = $event->getData();
            $form   = $event->getForm();

            // Recherche sur champs multiples (saisie libre)
            if($this->addField(DataSearch::WHATEVER, $search)) {
                $form->add(DataSearch::WHATEVER, TextType::class, array(
                    'label' => 'Vos critères de recherche'
                ));
            }

            // Référence du produit
            if($this->addField(DataSearch::PRODUCT_REF, $search)) {
                $form->add(DataSearch::PRODUCT_REF, TextType::class, array(
                    'label' => 'Référence produit'
                ));
            }

            // Référence de commande
            if($this->addField(DataSearch::ORDER_REF, $search)) {
                $form->add(DataSearch::ORDER_REF, TextType::class, array(
                    'label' => 'Référence commande'
                ));
            }

            // Nom du produit
            if($this->addField(DataSearch::PRODUCT_NAME, $search)) {
                $form->add(DataSearch::PRODUCT_NAME, TextType::class, array(
                    'label' => 'Nom produit'
                ));
            }

            // Catégories de produits (liste déroulante)
            if($this->addField(DataSearch::PRODUCT_CATEGORY, $search)) {
                $form->add(DataSearch::PRODUCT_CATEGORY, ChoiceType::class, array(
                    'label'         => 'Catégorie de produit',
                    'choices'       => $this->buildProductCategoryChoices(),
                    'empty_data'    => null,
                    'required'      => false
                ));
            }

        });
    }

    public function configureOptions(OptionsResolver $resolver) {
        
        // Ne pas renseigner de classe pour ce formulaire !
        // Il s'agit d'un formulaire générique pour afficher des critères de recherche
        $resolver->setDefaults(array('csrf_protection' => false,));
        
    }
    
    /**
     * Préfixe utilisé pour nommer les éléments du formulaire
     * Toute modification doit être reportée dans les templates, codes JS et CSS
     * 
     * @return string
     */
    public function getBlockPrefix() {
        return 'ds';
    }
    
    /**
     * Valider qu'un champ doit être ajouté dans le formulaire
     * 
     * @param string    $id         Identifiant du champ 
     * @param array     $data       Données caractérisant la recherche 
     * 
     * @return boolean
     */
    private function addField($id, $data) {
        return array_key_exists($id, $data);
    }
    
    /**
     * Liste des catégories de produits
     * 
     * @return array
     */
    private function buildProductCategoryChoices() {
        $choices    = [];
        $categories = $this->productCategoryService->getProductCategories();

        foreach ($categories as $cat) {
            $choices[$cat->getName()] = $cat->getId();
        }

        return $choices;
    }


}