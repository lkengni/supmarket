<?php

namespace App\Form;

use App\Entity\ProductCategory;
use App\Entity\Product;
use App\Service\ProductCategoryService;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\TextAreaType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\Length;
use Symfony\Component\Validator\Constraints\NotBlank;

class ProductType extends AbstractType
{
    /**
     * Instance du service en charge des catégories de produits
     * 
     * @var ProductCategoryService 
     */
    private $productCategoryService;
    
    /**
     * Constructeur
     */
    public function __construct(ProductCategoryService $productCategoryService) {
        $this->productCategoryService = $productCategoryService;
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        // Nom
        $builder->add('name', TextType::class, [
            'label' => 'Nom*',
            'constraints' => [
                new NotBlank([
                    'message' => 'Ce champ ne peut être vide'
                ])
            ]
        ]);

        // Référence
        $builder->add('reference', TextType::class, [
            'label' => 'Référence*',
            'constraints' => [
                new NotBlank([
                    'message' => 'Ce champ ne peut être vide'
                ])
            ]
        ]);

        // Prix
        $builder->add('price', TextType::class, [
            'label' => 'Prix*',
            'required'  => true,
            'constraints' => [
                new NotBlank([
                    'message' => 'Ce champ ne peut être vide'
                ])
            ]
        ]);

        // Quantité
        $builder->add('quantity', TextType::class, [
            'label' => 'Quantité*',
            'required'  => true,
            'constraints' => [
                new NotBlank([
                    'message' => 'Ce champ ne peut être vide'
                ])
            ]
        ]);

        // Description
        $builder->add('description', TextType::class, array(
            'label'     => 'Description',
            'required'  => false,
            'attr'      => array('rows' => '5')
        ));

        // Catégorie
        $product_category_options = array(
            'label'         => 'Catégorie*',
            'class'         => ProductCategory::class,
            'choices'       => $this->buildProductCategoryChoices(),
            'choice_label'  => 'name',
            'empty_data'    => null,
            'required'      => true
        );
        $builder->add('category', EntityType::class, $product_category_options);

        // Bouton Envoyer
        $builder->add('submit', SubmitType::class, array(
            'label' => 'Enregistrer'
        ));
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Product::class,
        ]);
    }

    /**
     * Liste des catégories de produits
     * 
     * @return array
     */
    private function buildProductCategoryChoices() {
        return $this->productCategoryService->getProductCategories();
    }

}
