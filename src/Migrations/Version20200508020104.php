<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20200508020104 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE product (id INT AUTO_INCREMENT NOT NULL, category_id INT NOT NULL, created_by_id INT NOT NULL, updated_by_id INT NOT NULL, reference VARCHAR(255) NOT NULL, name VARCHAR(255) NOT NULL, description VARCHAR(255) DEFAULT NULL, price DOUBLE PRECISION NOT NULL, created_at DATETIME NOT NULL, updated_at DATETIME NOT NULL, INDEX IDX_D34A04AD12469DE2 (category_id), INDEX IDX_D34A04ADB03A8386 (created_by_id), INDEX IDX_D34A04AD896DBBDE (updated_by_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE product ADD CONSTRAINT FK_D34A04AD12469DE2 FOREIGN KEY (category_id) REFERENCES product_category (id)');
        $this->addSql('ALTER TABLE product ADD CONSTRAINT FK_D34A04ADB03A8386 FOREIGN KEY (created_by_id) REFERENCES user (id)');
        $this->addSql('ALTER TABLE product ADD CONSTRAINT FK_D34A04AD896DBBDE FOREIGN KEY (updated_by_id) REFERENCES user (id)');
        $this->addSql('ALTER TABLE product_category DROP FOREIGN KEY FK_CDFC7356896DBBDE');
        $this->addSql('ALTER TABLE product_category DROP FOREIGN KEY FK_CDFC7356B03A8386');
        $this->addSql('DROP INDEX IDX_CDFC7356896DBBDE ON product_category');
        $this->addSql('DROP INDEX IDX_CDFC7356B03A8386 ON product_category');
        $this->addSql('ALTER TABLE product_category ADD created_by_id INT NOT NULL, ADD updated_by_id INT NOT NULL, DROP created_by, DROP updated_by');
        $this->addSql('ALTER TABLE product_category ADD CONSTRAINT FK_CDFC7356896DBBDE FOREIGN KEY (updated_by_id) REFERENCES user (id)');
        $this->addSql('ALTER TABLE product_category ADD CONSTRAINT FK_CDFC7356B03A8386 FOREIGN KEY (created_by_id) REFERENCES user (id)');
        $this->addSql('CREATE INDEX IDX_CDFC7356896DBBDE ON product_category (updated_by_id)');
        $this->addSql('CREATE INDEX IDX_CDFC7356B03A8386 ON product_category (created_by_id)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('DROP TABLE product');
        $this->addSql('ALTER TABLE product_category DROP FOREIGN KEY FK_CDFC7356B03A8386');
        $this->addSql('ALTER TABLE product_category DROP FOREIGN KEY FK_CDFC7356896DBBDE');
        $this->addSql('DROP INDEX IDX_CDFC7356B03A8386 ON product_category');
        $this->addSql('DROP INDEX IDX_CDFC7356896DBBDE ON product_category');
        $this->addSql('ALTER TABLE product_category ADD created_by INT NOT NULL, ADD updated_by INT NOT NULL, DROP created_by_id, DROP updated_by_id');
        $this->addSql('ALTER TABLE product_category ADD CONSTRAINT FK_CDFC7356B03A8386 FOREIGN KEY (created_by) REFERENCES user (id)');
        $this->addSql('ALTER TABLE product_category ADD CONSTRAINT FK_CDFC7356896DBBDE FOREIGN KEY (updated_by) REFERENCES user (id)');
        $this->addSql('CREATE INDEX IDX_CDFC7356B03A8386 ON product_category (created_by)');
        $this->addSql('CREATE INDEX IDX_CDFC7356896DBBDE ON product_category (updated_by)');
    }
}
