USE `supmarket`;

SET NAMES utf8;

ALTER TABLE `product_category` CHANGE `created_by_id` `created_by` INT(11) NOT NULL;
ALTER TABLE `product_category` CHANGE `updated_by_id` `updated_by` INT(11) NOT NULL;