USE `supmarket`;

SET NAMES utf8;

ALTER TABLE `product` CHANGE `category_id` `category` INT(11) NOT NULL;
ALTER TABLE `product` CHANGE `created_by_id` `created_by` INT(11) NOT NULL;
ALTER TABLE `product` CHANGE `updated_by_id` `updated_by` INT(11) NOT NULL;
ALTER TABLE `product` CHANGE `description` `description` TEXT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL;
ALTER TABLE `product` CHANGE `price` `price` DOUBLE NOT NULL DEFAULT '0';
