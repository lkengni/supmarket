USE `supmarket`;

SET NAMES utf8;

ALTER TABLE `supmarket`.`user` ADD `passwordToken` VARCHAR(255) NULL DEFAULT NULL AFTER `first_name`;