USE `supmarket`;

SET NAMES utf8;

INSERT INTO `product_category` (`id`, `name`, `created_at`, `updated_at`, `created_by`, `updated_by`) 
VALUES (1, 'Fruits et légumes', '2020-06-08 14:55:58', '2020-06-08 14:55:58', '2', '2'), 
(2, 'Pain et pâtisseries', '2020-06-08 15:07:44', '2020-06-08 15:07:44', '2', '2'), 
(3, 'Boissons', '2020-06-08 15:07:44', '2020-06-08 15:07:44', '2', '2'), 
(4, 'Électroménager', '2020-06-08 15:07:44', '2020-06-08 15:07:44', '2', '2'), 
(5, 'Hygiène et beauté', '2020-06-08 15:07:44', '2020-06-08 15:07:44', '2', '2');