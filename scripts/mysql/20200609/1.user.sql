USE `supmarket`;

SET NAMES utf8;

ALTER TABLE `user` ADD `adresse` VARCHAR(255) NULL DEFAULT NULL 
AFTER `first_name`, ADD `birth_date` DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP AFTER `adresse`;

ALTER TABLE `user` ADD `card_number` BIGINT(11) NULL DEFAULT NULL AFTER `birth_date`;

ALTER TABLE `user` ADD `card_expiration_date` DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP() 
AFTER `card_number`, ADD `card_cvv` INT NOT NULL AFTER `card_expiration_date`;

ALTER TABLE `user` ADD `account` FLOAT NOT NULL DEFAULT '0' AFTER `card_cvv`;