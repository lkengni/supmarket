USE `supmarket`;

SET NAMES utf8;

CREATE TABLE `supmarket`.`orders` 
( `id` INT NOT NULL AUTO_INCREMENT , 
`reference` BIGINT NOT NULL , `products` LONGTEXT NOT NULL COMMENT '(DC2Type: json)' , 
`total_ht` FLOAT NOT NULL DEFAULT '0' , `total_ttc` FLOAT NOT NULL DEFAULT '0' , 
`tva` FLOAT NOT NULL DEFAULT '0.2' , `created_at` DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP , 
`created_by` INT NOT NULL , `updated_at` DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP , 
`updated_by` INT NOT NULL , PRIMARY KEY (`id`)) 
ENGINE = InnoDB;
